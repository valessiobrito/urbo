class Admin::OfficialDiaryConfigurationsController < Admin::ResourcesController

  def cover
    get_object
    send_data @item.cover, type: 'image/jpeg', filename: 'cover.jpg'
  end

  def wallpaper
    get_object
    send_data @item.wallpaper, type: 'image/jpeg', filename: 'wallpaper.jpg'
  end

end
