class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.integer :official_diary_id

      t.binary :data
      t.text :text_data
      t.string :content_type
      t.string :original_filename

      t.timestamps
    end
  end
end
