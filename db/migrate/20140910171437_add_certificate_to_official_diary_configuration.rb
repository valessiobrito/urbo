class AddCertificateToOfficialDiaryConfiguration < ActiveRecord::Migration
  def change
    add_column :official_diary_configurations, :certificate, :text
  end
end
