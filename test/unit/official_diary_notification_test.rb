require_relative "../test_helper"

class OfficialDiaryNotificationTest < ActiveSupport::TestCase

  test 'saves notification date to official diary' do
    diary = create_official_diary.publish!
    OfficialDiaryNotification.send_all_pending
    diary.reload
    assert_not_nil diary.notification_sent_at
  end

  test 'sends (only one) email' do
    create_official_diary.publish!
    Subscription.create!(email: 'foo@bar.com', enabled: true)
    assert_mail_sent(1) do
      OfficialDiaryNotification.send_all_pending
      OfficialDiaryNotification.send_all_pending
    end
  end

  test 'sends only to enabled subscribers' do
    create_official_diary.publish!
    Subscription.create!(email: 'foo@example.com', enabled: true)
    Subscription.create!(email: 'bar@example.com', enabled: false)
    assert_mail_sent(1) do
      OfficialDiaryNotification.send_all_pending
    end
  end

end
