# encoding: UTF-8

class SubscriptionMailer < ActionMailer::Base

  def confirm_subscription(subscription)
    config = OfficialDiaryConfiguration.current
    @link = "%s://%s/subscriptions/%s" % [
      config.protocol,
      config.hostname,
      subscription.token,
    ]
    mail(
      to: subscription.email,
      from: config.mail_from,
      subject: _('Confirme sua inscrição'),
    )
  end

end
