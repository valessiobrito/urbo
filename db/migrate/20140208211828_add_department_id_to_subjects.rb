class AddDepartmentIdToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :department_id, :integer
  end
end
