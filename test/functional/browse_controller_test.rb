require_relative "../test_helper"
require 'minitest/mock'

class BrowseControllerTest < ActionController::TestCase

  test 'index' do
    get :index
    assert_response :success
  end

  test "latest month at index" do
    Date.stub :today, Date.new(2013,9,1) do
      get :index
    end

    assert_equal [2013,9], period.to_a
  end

  test 'explicit month' do
    get :index, :year => '2012', :month => '08'
    assert_equal [2012, 8], period.to_a
  end

  test 'gets list of official diaries' do
    list = [official_diaries(:one)]
    OfficialDiary.stub :browse, list do
      get :index
    end
    assert_equal list, assigns(:official_diaries)
  end

  test 'download' do
    official_diary = create_official_diary({}).publish!
    date = official_diary.date
    get :download, year: date.year, month: date.month, day: date.day

    assert_equal 'application/pdf', @response.content_type
  end

  private

  def period
    assigns(:period)
  end

end
