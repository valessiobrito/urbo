# encoding: UTF-8

if OfficialDiaryConfiguration.count == 0
  config = OfficialDiaryConfiguration.new
  config.number_of_columns = 2
  config.preamble = 'Este é o conteúdo do preâmbulo (texto depois da capa, mas antes do sumário). Ele pode ser modificado em Cadastros → Configurações'
  config.header = 'Este é o conteúdo do cabeçalho. Ele pode ser modificado em Cadastros → Configurações'
  config.footer = 'Este é o conteúdo do rodapé. Ele pode ser modificado em Cadastros → Configurações'
  config.save!
end

Book.create(name: 'Atos Oficiais')
Book.create(name: 'Licitações')
Book.create(name: 'Contas Públicas')
Book.create(name: 'Notícia')
Book.create(name: 'Outros', position: 99)
Book.create(name: 'Responsabilidade Fiscal')
Book.create(name: 'Transparência Pública')
Book.create(name: 'Contratações')

[
  "Procuradora Geral do Município",
  "Gabinete Civil",
  "Secretaria Municipal de Comunicação",
  "Secretaria Municipal de Governo",
  "Secretaria Municipal de Desenvolvimento Social",
  "Secretaria Municipal de Educação",
  "Secretaria Municipal de Trabalho, Renda e Desenvolvimento Econômico SEMTRE",
  "Secretaria de Infraestrutura UrbanaSecretaria Municipal da Transparência e Controle",
  "Secretaria Municipal de Finanças",
  "Secretaria Municipal de Serviços Públicos",
  "Secretaria Municipal de Saúde",
  "Secretaria Municipal de Mobilidade Urbana",
  "Secretaria Municipal de Meio Ambiente",
  "Secretaria Municipal de Agricultura e Desenvolvimento Rural",
  "Secretaria Municipal de Cultura, Turismo, Esporte e Lazer",
  "Empresa Municipal de Urbanização de Vitória da Conquista (Emurc)",
  "Ouvidoria Geral do Municipio",
  "Programa Territórios Municipais da Cidadania",
  "Programa Territórios Municipais da Cidadania",
  "Gerência de Patrimônio",
  "Gerência de Cargos e Salários",
  "Coordenação de Politicas de Inclusão Social – Bolsa Familia",
  "Coordenadora de Material e Patrimônio",
  "Gerência de Comunicação e Zeladoria",
  "Coordenação de Recursos Humanos",
  "Gerencia de Segurança Patrimonial",
  "Gerencia de Almoxarifado",
  "Diretor Geral da Fundação Pública de Saúde (FSVC)",
  "Gerencia de Compras",
  "Coordenação do Arquivo Público Municipal",
  "Conselho  Municipal de Alimentação Escolar – CAE",
  "Conselho Municipal de Assistência Social - CMAS",
  "Conselho Municipal de Cultura",
  "Conselho Municipal dos Direitos da Criança e do Adolescente",
  "Conselho Municipal de Economia Solidária",
  "Conselho Municipal de Educação",
  "Conselho do Fundo de Manutenção e Desenvolvimento da Educação Básica e de Valorização dos Profissionais da Educação - FUNDEB",
  "Conselho Municipal de Habitação Popular",
  "Conselho Municipal do Idoso",
  "Conselho Municipal da Igualdade Racial",
  "Conselho Municipal da Juventude de Vitória da Conquista",
  "Conselho Municipal de Meio Ambiente",
  "Conselho Municipal da Mulher",
  "Conselho do Orçamento Participativo",
  "Conselho Municipal dos Direitos da Pessoa com Deficiência",
  "Conselho de Segurança Alimentar e Nutricional",
  "Conselho Municipal de Transportes Públicos",
  "Conselho Tutelar",
  "Conselho de Saúde",
  "Outros",
  "Fundação de Saúde de Vitória da Conquista"
].each do |dep|
  Department.create(name: dep)
end

$text = File.readlines('test/fixtures/lorem.txt').map(&:strip)

$months_back = 29

$start_date = Date.today - ($months_back-1).months

n = 0
$months_back.times do |i|
  ref_date = $start_date + i.months
  day = 1
  (1+rand(3)).times.each do
    n += 1
    day += (1+rand(2))
    d = OfficialDiary.new(
      number: n,
      date: Date.new(ref_date.year, ref_date.month, day),
    )
    if d.save
      puts 'CREATE OfficialDiary'
      (1+rand(10)).times do
        subject = Subject.new
        subject.book = Book.first(order: 'random()')
        subject.department = Department.first(order: 'random()')
        subject.official_diary = d
        subject.title = $text.sample
        subject.contents = (1..30).map { $text.sample }.join(' ')
        if subject.save
          puts 'CREATE Subject'
        end
      end

      attachment = Attachment.new
      attachment.official_diary_id = d.id
      attachment.generate = true
      attachment.save!
      puts 'CREATE Attachment'

      if d.date <= Date.today
        d.publish!
      end
    end
  end
end
