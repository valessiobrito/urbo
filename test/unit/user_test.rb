require_relative "../test_helper"
require 'digest/sha2'

class UserTest < ActiveSupport::TestCase

  test "requires a password" do
    user = User.new
    user.password = '123456'
    user.password_confirmation = '654321'
    user.valid?

    assert user.errors[:password].any?
  end

  test 'hash password' do
    user = User.new
    user.password = user.password_confirmation = 'test'
    user.save
    assert_not_nil user.hashed_password
    assert_equal 64, user.hashed_password.length
  end

  test 'use random salt' do
    user1 = User.new.tap { |u| u.password = u.password_confirmation = 'test'; u.save }
    user2 = User.new.tap { |u| u.password = u.password_confirmation = 'test'; u.save }
    assert_not_equal user1.hashed_password, user2.hashed_password
  end

  test 'authenticate' do
    user = User.new
    user.password = user.password_confirmation = 'test'
    user.username = 'foobar'
    user.email = 'foobar@example.com'
    user.save!

    assert_nil User.authenticate('foobar', 'wrongpassword')
    assert_equal user, User.authenticate('foobar', 'test')
  end

  test 'authenticate unknown user' do
    assert !(User.authenticate('unknownuser', 'anypassword'))
  end

  test 'default role' do
    assert_equal 'editor', User.new.role
  end

end
