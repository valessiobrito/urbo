class CreateOfficialDiaryConfigurations < ActiveRecord::Migration
  def change
    create_table :official_diary_configurations do |t|
      t.integer :number_of_columns, :default => 1
      t.integer :font_size,         :default => 12
      t.text    :header
      t.text    :footer
      t.text    :preamble
      t.timestamps
    end
  end
end
