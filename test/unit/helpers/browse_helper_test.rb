require_relative "../../test_helper"
require 'minitest/mock'

class BrowseHelperTest < ActionView::TestCase

  test 'period title' do
    assert_equal "Janeiro de 2013", period_title(Period(2013,1))
  end

  test 'month selector data' do
    list = []
    Date.stub :today, Date.new(2013,9,1) do
      list += month_selector_data(Period(2013,8))
    end
    assert !list.include?(['Dez', 12, nil]), "should not include months in the future"
    assert list.include?(['Ago', 8, :active]), "should mark the currently active month"
    assert list.include?(['Jul', 7, nil]), "should include month in the past"
    assert_equal ['Jan', 1, nil], list.first, "should sort with earlier months first"
  end

  private

  def _(s)
    s
  end

end
