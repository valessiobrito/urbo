class AddTitleToExtraFile < ActiveRecord::Migration
  def change
    add_column :extra_files, :title, :string
  end
end
