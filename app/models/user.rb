require 'securerandom'
require 'digest/sha2'

class User < ActiveRecord::Base

  belongs_to :department

  validates_uniqueness_of :username
  validates_presence_of :username
  validates_presence_of :email
  validates_format_of :email, :with => /@/

  attr_accessor :password
  attr_accessor :password_confirmation
  validates_confirmation_of :password
  validates_presence_of :password, :on => :create

  after_validation do |user|
    user.salt ||= SecureRandom.hex
    user.hashed_password = hash_password(user.password)
  end

  def hash_password(password)
    Digest::SHA256.hexdigest(salt + password)
  end

  def self.authenticate(username, password)
    user = User.find_by_username(username)
    if user && user.hash_password(password) == user.hashed_password
      user
    else
      # FIXME return a null object?
      nil
    end
  end

  include Typus::Orm::ActiveRecord::User::InstanceMethods

  serialize :preferences, Hash

  validates_presence_of :role
  validates_inclusion_of :role, :in => ['editor', 'reviewer', 'collaborator','admin']

  def self.roles
    [
      [_('Editor'), 'editor'],
      [_('Revisor'), 'reviewer'],
      [_('Colaborador'), 'collaborator'],
      [_('Administrator'), 'admin'],
    ]
  end

  def self._(s)
    # FIXME
    s
  end

end
