//= require jquery-ui

jQuery(function($) {
  $('.datepicker').datepicker({ "dateFormat": "dd/mm/yy" });

  $('#add-extra-file').click(function() {
    var html = '';
    html += '<tr>'
    html += '<td>'
    html += $('#template-title-tag').html();
    html += '</td>'
    html += '<td>'
    html += $('#template-file-tag').html();
    html += '</td>'
    html += '<td>'
    html += '<a href="#" onclick="javascript: $(this).closest(\'tr\').remove(); return false">Remover</a>'
    html += '</td>'
    html += '</tr>'

    $(html).insertBefore($(this).closest('tr'));
    return false;
  });

  $('td').each(function() {
    if ($(this).html().match('Publicado')) {
      $(this).wrapInner('<span class="published"></span>');
    }
  });

});
