class AddDateToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :date, :date
    execute 'UPDATE attachments SET date = official_diaries.date FROM official_diaries WHERE official_diaries.id = attachments.official_diary_id;'
  end
end
