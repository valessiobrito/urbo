# encoding: UTF-8

require_relative "../test_helper"
require 'tmpdir'
require 'timeout'

class OfficialDiaryPdfTest < ActiveSupport::TestCase

  def setup
    @tmpdir = Dir.mktmpdir
    @tmppdf = File.join(@tmpdir, 'the.pdf')
  end

  attr_reader :tmppdf

  def teardown
    FileUtils.rm_rf(@tmpdir)
  end

  test 'creates PDF file' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.create!(tmppdf)

    assert_valid_pdf tmppdf
  end

  test 'converts HTML to LaTeX' do
    pdf = OfficialDiaryPdf.new(nil)
    assert_equal 'A \textbf{bold} statement', pdf.html_to_latex('A <b>bold</b> statement').strip
  end

  test 'official diary with actual data' do
    Subject.new(contents: 'lorem ipsum').tap do |s|
      s.book = Book.first
      diary.subjects << s
    end
    Subject.new(contents: 'dorem armet').tap do |s|
      s.book = Book.last
      diary.subjects << s
    end

    pdf = OfficialDiaryPdf.new(diary)
    pdf.create!(tmppdf)

    assert_valid_pdf tmppdf
  end

  test 'official diary with several types of markup' do
    Subject.new(contents: "<p>Isto &eacute; um teste. <strong>Negrito</strong> <em>it&aacute;lico</em> <u>sublinhado</u> e outras coisas.</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Tabela</td>\r\n\t\t\t<td>Outra coisa</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>pau</td>\r\n\t\t\t<td>pedra</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>fim do caminho</td>\r\n\t\t\t<td>peido</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<blockquote>\r\n<p>Uma cita&ccedil;&atilde;o muito bala.</p>\r\n</blockquote>\r\n\r\n<p>asdhsakjdhsakdhaskdhlasd gs dflas flsad fhlsakd fhsladk flsdka fhlas flsahd flsadk fhaslkd fhsdaklf asdlfg sdahfg sdakjf gsdafg sadjkf sakdjf gaskjfdg sda.</p>\r\n\r\n<p>Muito bala!</p>\r\n").tap do |s|
      s.book = Book.first
      diary.subjects << s
    end

    pdf = OfficialDiaryPdf.new(diary)

    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end

    assert_valid_pdf tmppdf
  end

  test 'makes a configuration instance available' do
    pdf = OfficialDiaryPdf.new(diary)
    assert pdf.config
  end

  test 'has multicol support' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.number_of_columns = 2
    assert_match /\\begin\{multicols\*\}\{#{pdf.config.number_of_columns}\}/, pdf.latex_source
  end

  test 'uses font size from configuration' do
    pdf = OfficialDiaryPdf.new(diary)
    assert_match /\\documentclass\[a4paper,#{pdf.config.font_size}pt\]\{extarticle\}/, pdf.latex_source
  end

  test 'use configured header' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.header = 'header text'
    assert_match /header text/, pdf.latex_source
  end

  test 'use configured footer' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.footer = 'footer text'
    assert_match /\\lfoot\{.*footer text\}/, pdf.latex_source
  end

  test 'use configured preamble' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.preamble = 'preamble text'
    assert_match /preamble text/, pdf.latex_source
  end

  PROBLEMATIC_TABLE = "<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Coluna 1</td>\r\n\t\t\t<td>Coluna 2</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>conteudo 1</td>\r\n\t\t\t<td>\r\n\t\t\t<p>conteudo 2</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>conteudo 3</td>\r\n\t\t\t<td>conteudo 4</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n"

  test 'does not mess tables up' do
    diary.subjects.last.contents = PROBLEMATIC_TABLE
    pdf = OfficialDiaryPdf.new(diary)

    assert_no_match /Coluna 1\s*Coluna 2/, pdf.latex_source
  end

  # new pandoc versions use longtable for tables, which is not compatible with
  # multicolumn environments
  test 'actually generates PDF with table in 2-column mode' do
    diary.subjects.last.contents = PROBLEMATIC_TABLE
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.stub :number_of_columns, 2 do
      pdf.generate!
    end
  end

  test 'support for arbitrary unicode characters' do
    pdf = OfficialDiaryPdf.new(diary)
    diary.subjects.first.contents = 'Some unicode test: áéíóú → ¹² ∞'

    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end

    assert_valid_pdf tmppdf
  end

  test 'with header coming from HTML' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.header = '<p>Some HTML text</p>'
    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end
    assert_valid_pdf tmppdf
  end

  test 'header with multiple paragraphs' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.header = '<p>Some HTML text</p><p>Another paragraph</p>'
    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end
    assert_valid_pdf tmppdf
  end

  test 'footer with multiple paragraphs' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.footer = '<p>Some HTML text</p><p>Another paragraph</p>'
    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end
    assert_valid_pdf tmppdf
  end

  test 'cover' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.cover = File.read(fixture_file_upload('files/white.jpg'), mode: 'rb')
    assert_pdf_created pdf
  end

  test 'wallpaper' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.config.wallpaper = File.read(fixture_file_upload('files/white.jpg'), mode: 'rb')
    assert_pdf_created pdf
  end

  test 'with extra files' do
    subject = diary.subjects.first
    subject.extra_files << ExtraFile.new(upload: fixture_file_upload('files/test.odt'))

    pdf = OfficialDiaryPdf.new(diary)
    assert_pdf_created pdf
  end

  test 'with extra files that have spaces in their filenames' do
    subject = diary.subjects.first
    subject.extra_files << ExtraFile.new(upload: fixture_file_upload('files/test with spaces.txt'))
    pdf = OfficialDiaryPdf.new(diary)
    assert_pdf_created pdf
  end

  test 'raise exception when PDF generation fails' do
    pdf = OfficialDiaryPdf.new(diary)
    pdf.stub(:create!, nil) do
      assert_raise OfficialDiaryPdf::GenerationFailure do
        pdf.generate!
      end
    end
  end

  %w[
    abcdefghijklmnopqrstuxywz0123456789
    aéíóúaéíóúaéíóúaéíóúaéíóúaéíóúaéíóú
    antinconstitucionalissimamente
  ].each do |longword|
    test "filter #{longword} to stop it from overflowing the margin in text" do
      text = "Here is one word that is too long: #{longword}"
      pdf = OfficialDiaryPdf.new(diary)
      pdf.filter!(text)
      assert_equal "Here is one word that is too long: \\seqsplit{#{longword}}", text
      assert_pdf_created pdf
    end
  end

  %w[
    http://www.ba.tmunicipal.org.br/prefeitura/vitoriadaconquista/publicacao
    www.somelongsitename.com
  ].each do |url|
    test "Mark long URL #{url} with seqsplit" do
      text = "Here is a URL:\n\n#{url}"
      pdf = OfficialDiaryPdf.new(diary)
      pdf.filter!(text)
      assert_equal "Here is a URL:\n\n\\seqsplit{#{url.gsub('/', '\slash{}')}}", text
      assert_pdf_created pdf
    end
  end

  test 'url with non-ascii characters' do
    text = 'Take this URL: www.licitações-e.com.br'
    pdf = OfficialDiaryPdf.new(diary)
    pdf.filter!(text)

    assert_equal 'Take this URL: \\seqsplit{www.licita{ç}{õ}es-e.com.br}', text
    assert_pdf_created pdf
  end

  test 'Tell LaTeX to hyphenate words with slashes' do
    text = 'ONEWORD/ANOTHERWORD'
    pdf = OfficialDiaryPdf.new(diary)
    pdf.filter!(text)
    assert_equal 'ONEWORD\slash\hspace{0pt}ANOTHERWORD', text
    assert_pdf_created pdf
  end

  private

  def assert_pdf_created(pdf)
    Timeout.timeout(10) do
      pdf.create!(tmppdf)
    end
  end

  def assert_valid_pdf(filename)
    assert File.exists?(filename), "#{filename} does not exists!"
    assert_equal 'application/pdf', mime_type(tmppdf)
  end

  def diary
    @diary ||= OfficialDiary.new(date: Date.today).tap do |d|
      d.subjects << Subject.new(title: 'subj 1').tap { |s| s.book = Book.first }
      d.subjects << Subject.new(title: 'subj 2').tap { |s| s.book = Book.first }
    end
  end

end
