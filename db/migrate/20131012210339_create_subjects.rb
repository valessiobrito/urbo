class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.integer :official_diary_id, :null => false
      t.integer :book_id,           :null => false
      t.date    :date,              :null => false
      t.text :contents

      t.timestamps
    end
  end
end
