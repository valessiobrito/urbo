# Urbo - sistema de publicação de Diário Oficial

## Instruções para executar um ambiente de desenvolvimento

* Instalar vagrant
  * Obter uma máquina com Debian 7 (wheezy), chamada de `debian-wheezy`.
* Obter código-fonte [no Gitlab](http://gitlab.com/colivre/urbo)
* Crie o arquivo `config/environment.sh`. Ele serve para definir variáveis de
  ambiente antes da execução. Ex:
  
  ```
  # config/environment.sh

  hostname=foo.dev
  ```
  
* Executar `vagrant up`.
* Conectar na máquina virtual com `vagrant ssh`
* No diretório `/vagrant`, executar `rails s`
* Acessar interface web num navegador em [http://localhost:3000](http://localhost:3000/)
* Para rodar os testes, de dentro da VM entre no diretório `/vagrant` e execute o comanda `rake`

## Instruções para implantação

A implantação assume um servidor Debian versão 7 (wheezy).

### Implantação inicial

Antes de qualquer coisa, é necessário acesso SSH como root ao servidor (digamos
`servidor.xx.yy.gov.br`), e o SSH deve ser configurado da sequinte maneira (em
~/.ssh/config):

```
Host servidor.xx.yy.gov.br
  User root
```

Execute o script script/debian/bootstrap, passando o nome do servidor aonde o
sistema vai ser implantado, e.g.:

```
$ ./script/debian/bootstrap servidor.xx.yy.gov.br
```

Isso vai criar um git remote com o nome do servidor, fazer configurações
básicas necessárias, e fazer a implantação inicial.

### Atualizações

Se o processo de implantado inicial **não foi feito** a partir do mesmo
repositório onde você está trabalhando, então antes de qualquer coisa é
necessário configurar o _git remote_:

```
$ git remote add servidor.xx.yy.gov.br root@servidor.xx.yy.gov.br:/srv/dom-repository.git
```

A partir desse ponto, para fazer atualizações basta executar:

```
$ git push servidor.xx.yy.gov.br
```

**NOTA:** apenas mudanças que foram _commitadas_ no repositório git serão
implantadas.

## Copyright/Licença de uso

Copyright © 2013-2014 Prefeitura de Vitória da Conquista.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Please see the file COPYING for the full license text of the GNU General Public
License, version 3.
