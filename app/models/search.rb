class Search

  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :query

  def initialize(attrs=nil)
    attrs ||= {}
    attrs.each do |k,v|
      self.send("#{k}=", v)
    end
  end

  def results(page=nil)
    results = model.published.page(page)
    if !query.blank?
      results = results.search(query)
    end
    filters.each do |field|
      if !send(field.to_sym).blank?
        results = results.where(field => send(field.to_sym))
      end
    end
    preload.each do |association|
      results = results.preload(association)
    end
    results
  end

  def model
    Subject
  end

  attr_accessor :department_id, :book_id, :date, :number

  def filters
    [:department_id, :book_id, :date, :number]
  end

  def preload
    [:official_diary, :book, :department]
  end

  def blank?
    ([:query ] + filters).all? { |field| send(field).blank? }
  end

  def self.normalize(text)
    text.gsub(/(\d+)\.(\d+)/, '\1\2 \&').gsub(/\b0+(\d+)\b/, '\1 \&').gsub(/\b([\d\.]+)\/([\d\.]+)\b/, '\1 \2 \&')
  end

end
