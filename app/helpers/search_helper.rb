module SearchHelper

  # FIXME this will be probably useful also in other contexts
  class BootstrapPaginationRenderer < WillPaginate::ActionView::LinkRenderer

    def html_container(html)
      tag(:ul, html, :class => 'pagination')
    end

    def page_number(page)
      classname = nil
      if (page == current_page)
        classname = 'active'
      end
      tag(:li, tag(:a, page, :href => url(page)), :class => classname)
    end

    def previous_or_next_page(page, text, classname)
      if page
        tag(:li, tag(:a, text, :href => url(page)), :class => classname)
      else
        tag(:li, tag(:a, text, :href => '#'), :class => classname + ' disabled')
      end
    end
  end

  def result_window(collection)
    from = collection.offset + 1
    to = collection.offset + collection.length
    total = collection.total_entries
    _('%d resultados encontrados (mostrando de %d a %d).') % [total, from, to]
  end

  def paginate(collection)
    will_paginate collection, :renderer => SearchHelper::BootstrapPaginationRenderer, :previous_label => '&laquo;', :next_label => ('&raquo;')
  end

end
