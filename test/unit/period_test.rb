require_relative "../test_helper"

class PeriodTest < ActiveSupport::TestCase

  test 'basics' do
    assert_equal [2013,1], Period.new(2013,1).to_a
    assert_equal [2013,1], Period.new('2013','1').to_a
  end

  test 'current period' do
    assert_equal [2012,12], Period.new(nil, nil, Date.new(2012,12,1)).to_a
  end

  test 'requires year and month' do
    assert_raise ArgumentError do
      Period.new(2013)
    end
  end

  test 'helper function' do
    assert_equal [2013,1], Period('2013', '01').to_a
  end

  test 'helper function is idempotent' do
    p = Period.new
    assert_same p, Period(p)
  end

  test 'validate month' do
    assert_raise ArgumentError do
      Period.new(2013, 13)
    end
    (1..12).each { |m| Period.new(2013, m) }
  end

  test 'range' do
    assert_equal Range.new(Date.new(2013,1,1), Date.new(2013,1,31)), Period(2013, 1).range
    assert_equal Range.new(Date.new(2013,2,1), Date.new(2013,2,28)), Period(2013, 2).range
  end

end
