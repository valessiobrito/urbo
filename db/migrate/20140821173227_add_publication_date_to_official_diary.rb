class AddPublicationDateToOfficialDiary < ActiveRecord::Migration
  def change
    add_column :official_diaries, :publication_date, :datetime
  end
end
