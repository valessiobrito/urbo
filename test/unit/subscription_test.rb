require_relative "../test_helper"

class SubscriptionTest < ActiveSupport::TestCase
  test 'does not accept invalid email' do
    s = Subscription.new(email: 'invalid')
    s.valid?
    assert s.errors[:email].size > 0
    s.email = 'myself@example.com'
    s.valid?
    assert s.errors[:email].size == 0
  end

  test 'is not enabled by default' do
    assert !Subscription.new.enabled?
  end

  test 'has a random token' do
    s1 = Subscription.create!(email: 'foo@example.com')
    s2 = Subscription.create!(email: 'bar@example.com')
    assert_not_equal s1.token, s2.token
  end

  test 'generate a new token' do
    s = Subscription.create!(email: 'foo@example.com')
    token = s.token

    s.new_token!

    assert_not_equal token, Subscription.find(s.id).token
  end

  test "can't subscrice twice" do
    s1 = Subscription.create(email: 'foo@example.com')
    s2 = Subscription.new(email: 'foo@example.com')

    s2.valid?
    assert s2.errors[:email].size > 0
  end

  test 'Subscription#for: no email passed' do
    s = Subscription.for({})
    assert s.new_record?
  end

  test 'Subscription#for: email passed, existing record' do
    s1 = Subscription.create(email: 'foo@example.com')
    s2 = Subscription.for(email: 'foo@example.com')

    assert_equal s1, s2
  end

  test 'Subscription#for: email passed, does not exist' do
    s = Subscription.for(email: 'doesnotexist@doesnotexist.com')
    assert_equal 'doesnotexist@doesnotexist.com', s.email
  end

  test 'cancel' do
    s = Subscription.create(email: 'foo@bar.com')
    s.cancel!
    assert_equal false, s.enabled
  end

  test 'sends out confirmation email when created' do
    assert_mail_sent(1) do
      Subscription.create!(email: 'foo@example.com')
    end
  end

  test 'confirms' do
    s = Subscription.create(email: 'foo@example.com')
    s.confirm!
    s.reload
    assert s.enabled
  end

end
