class SearchController < ApplicationController

  def index
    @search = Search.new(params[:search])
    unless @search.date.blank?
      @search.date = Date.strptime(@search.date, I18n.translate('date.formats.default'))
    end
    unless @search.blank?
      @results = @search.results(params[:page])
    end
  end

end
