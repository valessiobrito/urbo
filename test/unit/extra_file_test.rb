require_relative "../test_helper"
require 'minitest/mock'

class ExtraFileTest < ActiveSupport::TestCase

  attr_reader :pdf
  def setup
    @tmpdir = Dir.mktmpdir
    @pdf = File.join(@tmpdir, 'the.pdf')
  end

  def teardown
    FileUtils.rm_rf(@tmpdir)
  end

  test 'gets file contents and metadata' do
    extra_file = ExtraFile.new(upload: fixture_file_upload('files/test.txt', 'text/plain'))
    assert_equal 'text/plain', extra_file.content_type
  end

  test 'gets filename' do
    extra_file = ExtraFile.new(upload: fixture_file_upload('files/test.txt', 'text/plain'))
    assert_equal 'test.txt', extra_file.filename
  end

  test 'get file date' do
    extra_file = ExtraFile.new(upload: fixture_file_upload('files/test.txt', 'text/plain'))
    assert extra_file.data.size > 0, "should obtain file contents"
  end

  def self.test_to_pdf(description, file)
    test "converts #{description} to PDF" do
      original_file = ExtraFile.new(upload: fixture_file_upload(file))
      original_file.convert_to_pdf(pdf)
      assert_equal 'application/pdf', mime_type(pdf)
    end
  end

  test_to_pdf 'Word document (docx) to PDF',        'files/test.docx'
  test_to_pdf 'Word document (doc) to PDF',         'files/test.doc'
  test_to_pdf 'Excel (xlsx) document to PDF',       'files/test.xlsx'
  test_to_pdf 'Excel (xls) document to PDF',        'files/test.xls'
  test_to_pdf 'LibreOffice Writer document to PDF', 'files/test.odt'
  test_to_pdf 'LibreOffice Calc document to PDF',   'files/test.ods'
  test_to_pdf 'images to PDF',                      'files/white.jpg'

  test 'leaves PDF files alone' do
    ExtraFile.new(upload: fixture_file_upload('files/test.pdf', 'application/pdf')).tap do |f|
      f.convert_to_pdf(pdf)
    end
    assert_equal fixture_file_upload('files/test.pdf').read, File.read(pdf, mode: 'rb')
  end

  test 'failed conversion raises an exception' do
    f = ExtraFile.new(upload: fixture_file_upload('files/test.txt'))
    f.stub(:conversion_command, ['sleep', '1h']) do
      f.stub(:conversion_timeout, 1) do
        assert_raise ExtraFile::ConversionToPdfFailed do
          f.convert_to_pdf(pdf)
        end
      end
    end
  end

  test 'remove spaces from filename when exporting' do
    extra_file = ExtraFile.new(upload: fixture_file_upload('files/test with spaces.txt', 'text/plain'))
    assert_no_match /\s/, extra_file.export_filename
  end

end
