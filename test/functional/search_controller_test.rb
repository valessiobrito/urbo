require_relative "../test_helper"

class SearchControllerTest < ActionController::TestCase

  test "should get index" do
    get :index
    assert_response :success
  end

  test 'actually searching' do
    get :index, search: { query: 'lorem' }
    assert_response :success
  end

  test 'search for attachment text' do
    get :index, search: { query: 'lorem' }
  end

end
