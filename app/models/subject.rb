# encoding: UTF-8

class Subject < ActiveRecord::Base
  attr_accessible :title, :contents, :official_diary_id, :book_id, :department_id, :page, :position, :add_files
  belongs_to :official_diary

  has_many :extra_files

  def published?
    self.official_diary.present? && self.official_diary.published?
  end

  scope :published, -> { includes(:official_diary).where ['official_diaries.publication_date <= ?', Time.now] }

  before_validation do |obj|
    if obj.official_diary
      obj.date = official_diary.date
      obj.number = official_diary.number
    end
  end

  belongs_to :book
  belongs_to :department
  validates_presence_of :book_id, :official_diary_id, :title
  validates_presence_of :contents, if: ->(s) { s.extra_files.empty? }

  before_save do |subject|
    subject.normalized_contents = Search.normalize(subject.contents)
  end

  def add_files
    []
  end

  def add_files=(files)
    files.each do |f|
      if f[:title].present? && f[:upload].present?
        self.extra_files << ExtraFile.new(f)
      end
    end
    self.extra_files
  end

  private

  def self.searchable_columns
    ['normalized_contents']
  end

end
